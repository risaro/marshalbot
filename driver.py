from marshalBot import print_t
import chromedriver_autoinstaller as ca
import os

print_t('Checking chromedriver version...')
chrome_version = ca.get_chrome_version()
print_t(f'Current chrome version: {chrome_version}')
if not ca.utils.check_version('chromedriver.exe', ca.utils.get_matched_chromedriver_version(chrome_version)):
    print_t('Outdated, updating...')
    webdriver_path = ca.install()
    os.replace(webdriver_path, 'chromedriver.exe')
    print_t('Successfully updated!')
else:
    print_t('Correct version, exiting...')
